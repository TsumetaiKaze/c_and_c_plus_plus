#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    int random  = rand() % 5;
    if (random==0) {
        ui->label->setText("Все будет!");
    }
    else if (random==1) {
        ui->label->setText("Будет новый!");
    }
    else {
        ui->label->setText("Будет!");
    }
}
